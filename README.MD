<table border="0">
 <tr>
    <td><img src="profile.jpg" width="150"/></td>
  <td> <h2><font color="Blue"> Mr.Vuttawat Uyanont </font></h2> <br>  
     <b>Graduated:</b> Bachelor of Engineering (Civil) from Thammasat University, 2006.  <br>
     <b>Studying:</b> Master Computer Science in Cybersecurity Management at Mahanakorn University.  </td>
 </tr>
</table>

## Careers
> + Starting a job as a civil engineer for 4-years; Airport Rail Link, buildings, oil and gas plant projects.  
> + Turn into the financial and banking field, *[Government Savings Bank](https://www.gsb.or.th)*, since 2009.  
> + From 2019, move to the insurance field, *[FWD Insurance Co., Ltd.](https://www.fwd.co.th)* as a digital distribution project manager.  

## Lives
> + Non-experience in direct-Cybersecurity. Have to coordinate some work with stakeholders.  
> + Intermediate level programming.  
> + Interested in IT & Tech, Banking, Financial, Real Estate, and Business.  
> + Looking for an opportunity in both Cybersecurity and Blockchain technology.  

## Hobbies
> + Crafted beer / Beer testing  
> + Trading Card Game  
> + Go  
